package com.zestworks.utils.coroutines

import kotlinx.coroutines.CoroutineDispatcher

data class CoroutineDispatcherProvider(
    val main : CoroutineDispatcher,
    val io : CoroutineDispatcher,
    val computation : CoroutineDispatcher
)