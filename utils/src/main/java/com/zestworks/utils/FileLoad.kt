package com.zestworks.utils

import android.content.Context
import com.google.gson.Gson
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStreamReader

fun <T> load(clazz: Class<T>, file: String): T {
    val fixtureStreamReader = InputStreamReader(clazz.classLoader!!.getResourceAsStream(file))
    return Gson().fromJson(fixtureStreamReader, clazz)
}

fun load(context: Context, file: Int): JSONObject {
    val jsonReader = BufferedReader(InputStreamReader(context.resources.openRawResource(file)))
    val jsonBuilder: StringBuilder = StringBuilder()
    var line: String? = null
    while (jsonReader.readLine().also { line = it } != null) {
        jsonBuilder.append(line).append("\n")
    }
    return JSONObject(jsonBuilder.toString())
}