package com.zestworks.utils.view

import android.widget.ImageView
import androidx.annotation.DrawableRes
import com.squareup.picasso.Picasso

fun ImageView.load(
    url: String,
    @DrawableRes placeholder: Int? = null,
    @DrawableRes errorDrawable: Int? = null
) {
    Picasso.get()
        .load(url)
        .also { picasso -> placeholder?.let { picasso.placeholder(placeholder) } }
        .also { picasso -> errorDrawable?.let { picasso.error(errorDrawable) } }
        .into(this)
}