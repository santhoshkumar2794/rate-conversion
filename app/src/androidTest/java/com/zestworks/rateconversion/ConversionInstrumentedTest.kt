package com.zestworks.rateconversion

import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.view.View
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.BoundedMatcher
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.jakewharton.espresso.OkHttp3IdlingResource
import com.zestworks.rateconversion.data.ConversionApi.Companion.okHttpClient
import com.zestworks.rateconversion.ui.ConversionViewHolder
import com.zestworks.rateconversion.ui.MainActivity
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.not
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ConversionInstrumentedTest {

    @get:Rule
    var activityRule: ActivityTestRule<MainActivity> = ActivityTestRule(MainActivity::class.java)

    private val idlingResource = OkHttp3IdlingResource.create("okHttp", okHttpClient)

    @Before
    fun setup() {
        IdlingRegistry.getInstance().register(idlingResource)
    }

    @After
    fun tearDown() {
        IdlingRegistry.getInstance().unregister(idlingResource)
    }

    @Test
    fun check_whether_items_are_loaded() {
        onView(withId(R.id.loaderView))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        onView(withId(R.id.conversionView))
            .check(ViewAssertions.matches(not(ViewMatchers.isDisplayed())))

        while (idlingResource.isIdleNow) {
            //Wait till the response
        }

        onView(withId(R.id.loaderView))
            .check(ViewAssertions.matches(not(ViewMatchers.isDisplayed())))

        onView(withId(R.id.conversionView))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun checking_whether_the_list_displayed_and_rotating_the_screen_maintains_data() {
        onView(withId(R.id.loaderView))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        while (idlingResource.isIdleNow) {
            //Wait till the response
        }

        onView(withId(R.id.conversionView))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        onView(withId(R.id.conversionView))
            .check { _, _ ->
                val atPosition = atPosition(2, withId(R.id.currencyName))
                atPosition!!.matches(withText("EUR"))
            }

        rotateScreen()

        onView(withId(R.id.loaderView))
            .check(ViewAssertions.matches(not(ViewMatchers.isDisplayed())))

        onView(withId(R.id.conversionView))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        onView(withId(R.id.conversionView))
            .check { _, _ ->
                val atPosition = atPosition(2, withId(R.id.currencyName))
                atPosition!!.matches(withText("EUR"))
            }

    }

    @Test
    fun clicking_on_an_item_should_update_the_base_currency() {
        onView(withId(R.id.loaderView))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        while (idlingResource.isIdleNow) {
            //Wait till the response
        }

        onView(withId(R.id.conversionView))
            .perform(RecyclerViewActions.actionOnItemAtPosition<ConversionViewHolder>(0, click()))

        onView(withId(R.id.conversionView))
            .check { _, _ ->
                val atPosition = atPosition(2, withId(R.id.currencyName))
                atPosition!!.matches(not(withText("EUR")))
            }
    }

    private fun rotateScreen() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        val orientation = context.resources.configuration.orientation

        val activity = activityRule.activity
        activity.requestedOrientation = if (orientation == Configuration.ORIENTATION_PORTRAIT)
            ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        else
            ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
    }

    private fun atPosition(position: Int, @NonNull itemMatcher: Matcher<View?>): Matcher<View?>? {
        return object : BoundedMatcher<View?, RecyclerView>(RecyclerView::class.java) {
            override fun describeTo(description: Description) {
                description.appendText("has item at position $position: ")
                itemMatcher.describeTo(description)
            }

            override fun matchesSafely(view: RecyclerView): Boolean {
                val viewHolder = view.findViewHolderForAdapterPosition(position)
                    ?: // has no item on such position
                    return false
                return itemMatcher.matches(viewHolder.itemView)
            }
        }
    }
}