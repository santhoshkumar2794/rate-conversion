package com.zestworks.rateconversion

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.nhaarman.mockitokotlin2.*
import com.zestworks.rateconversion.data.Currency
import com.zestworks.rateconversion.data.Symbol
import com.zestworks.rateconversion.domain.ConversionUseCase
import com.zestworks.rateconversion.viewmodel.Content
import com.zestworks.rateconversion.viewmodel.ConversionViewModel
import com.zestworks.rateconversion.viewmodel.Error
import com.zestworks.rateconversion.viewmodel.Loading
import com.zestworks.rateconversion.viewmodel.ViewState
import com.zestworks.utils.NetworkResult
import com.zestworks.utils.coroutines.CoroutineDispatcherProvider
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.produce
import kotlinx.coroutines.test.TestCoroutineDispatcher
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ConversionViewModelTest {

    private val useCase = Mockito.mock(ConversionUseCase::class.java)
    private lateinit var dispatcherProvider: CoroutineDispatcherProvider

    private val observer = mock<Observer<ViewState>>()

    private lateinit var viewModel: ConversionViewModel

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    @Before
    fun setup() {
        val dispatcher = TestCoroutineDispatcher()
        dispatcherProvider = CoroutineDispatcherProvider(
            main = dispatcher,
            io = dispatcher,
            computation = dispatcher
        )
        val testTicker = testTicker(CoroutineScope(dispatcher))
        viewModel = ConversionViewModel(useCase, dispatcherProvider, testTicker)
    }

    @After
    fun tearDown() {
        verifyNoMoreInteractions(useCase)
        viewModel.ratesLiveData.removeObserver(observer)
    }

    @Test
    fun `given a base currency and getConversionRates return a successful conversion list`() {
        runBlocking {
            val list = listOf(Currency(Symbol.EUR, 100f))
            Mockito.`when`(useCase.getConversionRates(any(), any())).thenReturn(
                NetworkResult.Success(list)
            )
            viewModel.ratesLiveData.observeForever(observer)
            assert(viewModel.ratesLiveData.value is Content)
            assert((viewModel.ratesLiveData.value as Content).rates == list)

            verify(useCase).getConversionRates(any(), any())
        }
    }

    @Test
    fun `given a base currency and getConversionRates returns an error`() {
        runBlocking {
            val errorMessage = "OOPS. Something went wrong"
            Mockito.`when`(useCase.getConversionRates(any(), any())).thenReturn(
                NetworkResult.Error(errorMessage = errorMessage)
            )
            viewModel.ratesLiveData.observeForever(observer)
            assert(viewModel.ratesLiveData.value is Error)
            assert((viewModel.ratesLiveData.value as Error).errorMessage == errorMessage)

            verify(useCase).getConversionRates(any(), any())
        }
    }

    @Test
    fun `clicking retry should returns a success`() {
        runBlocking {
            val list = listOf(Currency(Symbol.EUR, 100f))
            Mockito.`when`(useCase.getConversionRates(any(), any())).thenReturn(
                NetworkResult.Success(list)
            )
            viewModel.retry()
            viewModel.ratesLiveData.observeForever(observer)
            assert(viewModel.ratesLiveData.value is Content)
            assert((viewModel.ratesLiveData.value as Content).rates == list)

            verify(useCase).getConversionRates(any(), any())
        }
    }

    @Test
    fun `changing the base currency should return a new list`(){
        runBlocking {

            val newList = listOf(Currency(Symbol.INR, 100f))
            Mockito.`when`(useCase.getConversionRates(any(), any())).thenReturn(
                NetworkResult.Success(newList)
            )

            viewModel.onBaseSymbolChanged(Currency(Symbol.INR, 100f))
            viewModel.ratesLiveData.observeForever(observer)

            assert(viewModel.ratesLiveData.value is Content)
            assert((viewModel.ratesLiveData.value as Content).rates == newList)

            verify(useCase).getConversionRates(any(), any())
        }
    }

    @Test
    fun `changing the value should return a new list`(){
        runBlocking {
            val value = 40f
            val newList = listOf(Currency(Symbol.INR, value), Currency(Symbol.EUR,320f))
            Mockito.`when`(useCase.getConversionRates(any(), any())).thenReturn(
                NetworkResult.Success(newList)
            )

            viewModel.onBaseValueChanged(value)
            viewModel.ratesLiveData.observeForever(observer)

            assert(viewModel.ratesLiveData.value is Content)
            assert((viewModel.ratesLiveData.value as Content).rates == newList)

            verify(useCase).getConversionRates(any(), any())
        }
    }
}

fun testTicker(scope: CoroutineScope): ReceiveChannel<Unit> {
    return scope.produce {
        channel.send(Unit)
    }
}