package com.zestworks.rateconversion

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import com.zestworks.rateconversion.data.ConversionResponse
import com.zestworks.rateconversion.data.Currency
import com.zestworks.rateconversion.data.RatesRepository
import com.zestworks.rateconversion.data.Symbol
import com.zestworks.rateconversion.domain.ConversionUseCase
import com.zestworks.rateconversion.domain.ConversionUseCaseImpl
import com.zestworks.utils.NetworkResult
import com.zestworks.utils.load
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ConversionUseCaseTest {

    private val repository = mock<RatesRepository>()
    private val response = load(ConversionResponse::class.java, "conversion_response.json")
    private lateinit var useCase: ConversionUseCase

    @Before
    fun setup() {
        useCase = ConversionUseCaseImpl(repository)
    }

    @After
    fun tearDown() {
        verifyNoMoreInteractions(repository)
    }

    @Test
    fun `given a currency return a successful list`() {
        runBlocking {
            Mockito.`when`(repository.getConversionRates(any())).thenReturn(
                NetworkResult.Success(response)
            )

            val conversionRates = useCase.getConversionRates(Symbol.EUR.name, 10f)

            assert(conversionRates is NetworkResult.Success)
            val data = (conversionRates as NetworkResult.Success<List<Currency>>).data
            data.forEach {
                assert(it.value == (10f * response.getValueForSymbol(it.symbol)).toFloat())
            }

            verify(repository).getConversionRates(any())
        }
    }

    @Test
    fun `given a currency return an error`() {
        runBlocking {
            val errorMessage = "OOPS. Something went wrong"
            Mockito.`when`(repository.getConversionRates(any())).thenReturn(
                NetworkResult.Error(errorMessage)
            )

            val conversionRates = useCase.getConversionRates(Symbol.EUR.name, 10f)

            assert(conversionRates is NetworkResult.Error)
            assert((conversionRates as NetworkResult.Error).errorMessage == errorMessage)

            verify(repository).getConversionRates(any())
        }
    }

}