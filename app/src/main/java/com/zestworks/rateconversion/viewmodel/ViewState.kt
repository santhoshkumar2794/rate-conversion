package com.zestworks.rateconversion.viewmodel

import com.zestworks.rateconversion.data.Currency

sealed class ViewState
object Loading : ViewState()
data class Content(val rates: List<Currency>) : ViewState()
data class Error(val errorMessage: String) : ViewState()