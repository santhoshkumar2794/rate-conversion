package com.zestworks.rateconversion.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.zestworks.rateconversion.data.Currency
import com.zestworks.rateconversion.data.Symbol
import com.zestworks.rateconversion.domain.ConversionUseCase
import com.zestworks.utils.NetworkResult
import com.zestworks.utils.coroutines.CoroutineDispatcherProvider
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.launch
import javax.inject.Inject

class ConversionViewModel @Inject constructor(
    private val conversionUseCase: ConversionUseCase,
    private val dispatcher: CoroutineDispatcherProvider,
    private val ticker: ReceiveChannel<Unit>
) : ViewModel() {

    private var job = Job()
    private var scope = CoroutineScope(dispatcher.main + job)

    private val _baseCurrency =
        MutableLiveData<Currency>().apply { this.value = Currency(Symbol.EUR, 100f) }

    private val _ratesLiveData = MutableLiveData<ViewState>()
    val ratesLiveData: LiveData<ViewState> = Transformations.switchMap(_baseCurrency) {
        getConversionRate()
        _ratesLiveData
    }

    init {
        if (_ratesLiveData.value == null) {
            _ratesLiveData.value = Loading
        }
    }

    override fun onCleared() {
        super.onCleared()
        ticker.cancel()
    }

    fun onBaseSymbolChanged(currency: Currency) {
        _baseCurrency.value = currency.copy()
    }

    fun onBaseValueChanged(value: Float) {
        _baseCurrency.value = _baseCurrency.value!!.copy(value = value)
    }

    fun retry() {
        _baseCurrency.value = _baseCurrency.value
    }

    private fun getConversionRate() {
        resetJob()
        scope.launch(dispatcher.io) {
            loop@ for (unit in ticker) {
                val baseCurrency = _baseCurrency.value!!
                when (val rates =
                    conversionUseCase.getConversionRates(
                        base = baseCurrency.symbol.name,
                        value = baseCurrency.value
                    )) {
                    is NetworkResult.Success -> {
                        _ratesLiveData.postValue(Content(rates = rates.data))

                    }
                    is NetworkResult.Error -> {
                        _ratesLiveData.postValue(Error(errorMessage = rates.errorMessage))
                        break@loop
                    }
                }
            }
        }
    }

    private fun resetJob() {
        job.cancel()
        job = Job()
        scope = CoroutineScope(dispatcher.main + job)
    }
}