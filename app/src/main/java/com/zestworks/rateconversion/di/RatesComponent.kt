package com.zestworks.rateconversion.di


import com.zestworks.rateconversion.ui.MainActivity
import com.zestworks.rateconversion.ui.MainFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [NetworkModule::class, ViewModelModule::class, ConversionModule::class])
interface RatesComponent {

    fun inject(mainActivity: MainActivity): MainActivity

    fun inject(mainFragment: MainFragment): MainFragment
}