package com.zestworks.rateconversion.di

import com.zestworks.rateconversion.data.RatesRepository
import com.zestworks.rateconversion.data.RatesRepositoryImpl
import com.zestworks.rateconversion.domain.ConversionUseCase
import com.zestworks.rateconversion.domain.ConversionUseCaseImpl
import dagger.Binds
import dagger.Module

@Module
abstract class ConversionModule {

    @Binds
    abstract fun bindRatesRepository(ratesRepositoryImpl: RatesRepositoryImpl): RatesRepository

    @Binds
    abstract fun conversionUseCase(conversionUseCaseImpl: ConversionUseCaseImpl): ConversionUseCase
}