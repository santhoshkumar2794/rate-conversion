package com.zestworks.rateconversion.di

import com.zestworks.rateconversion.ui.MainActivity
import com.zestworks.rateconversion.ui.MainFragment

object RatesInjector {

    private fun bitCoinDaggerComponent() =
        DaggerRatesComponent.builder()
            .networkModule(NetworkModule())
            .build()!!

    fun inject(activity: MainActivity) {
        bitCoinDaggerComponent().inject(activity)
    }

    fun inject(mainFragment: MainFragment) {
        bitCoinDaggerComponent().inject(mainFragment)
    }
}