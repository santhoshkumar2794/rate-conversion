package com.zestworks.rateconversion.di

import com.zestworks.rateconversion.Constants
import com.zestworks.rateconversion.data.ConversionApi
import com.zestworks.rateconversion.data.ConversionApi.Companion.okHttpClient
import com.zestworks.utils.coroutines.CoroutineDispatcherProvider
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ObsoleteCoroutinesApi
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.ticker
import okhttp3.OkHttpClient
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient {
        return okHttpClient
    }

    @Provides
    @Singleton
    fun provideConversionApi(okHttpClient: OkHttpClient) =
        ConversionApi.create(okHttpClient = okHttpClient)

    @Provides
    @Singleton
    fun provideCoroutineDispatcher() =
        CoroutineDispatcherProvider(
            main = Dispatchers.Main,
            io = Dispatchers.IO,
            computation = Dispatchers.Default
        )

    @ObsoleteCoroutinesApi
    @Provides
    @Singleton
    fun provideTicker(coroutineDispatcherProvider: CoroutineDispatcherProvider): ReceiveChannel<Unit> =
        ticker(delayMillis = Constants.API_INTERVAL, context = coroutineDispatcherProvider.io)

}