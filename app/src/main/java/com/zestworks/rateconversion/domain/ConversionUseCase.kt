package com.zestworks.rateconversion.domain

import com.zestworks.rateconversion.data.ConversionResponse
import com.zestworks.rateconversion.data.Currency
import com.zestworks.rateconversion.data.RatesRepository
import com.zestworks.rateconversion.data.Symbol
import com.zestworks.utils.NetworkResult
import com.zestworks.utils.exhaustive
import javax.inject.Inject

interface ConversionUseCase {
    suspend fun getConversionRates(
        base: String,
        value: Float
    ): NetworkResult<List<Currency>>
}

class ConversionUseCaseImpl @Inject constructor(
    private val repository: RatesRepository
) :
    ConversionUseCase {
    override suspend fun getConversionRates(
        base: String,
        value: Float
    ): NetworkResult<List<Currency>> {
        return when (val conversionRates = repository.getConversionRates(base)) {
            is NetworkResult.Success -> {
                val conversionList = convertForValue(conversionRates.data, value)
                NetworkResult.Success(conversionList)
            }
            is NetworkResult.Error -> return conversionRates
        }
    }

    private fun convertForValue(
        conversionResponse: ConversionResponse,
        value: Float
    ): List<Currency> {
        val conversionList = ArrayList<Currency>()
        val rates = conversionResponse.rates

        Symbol.values().forEach {
            if (it == conversionResponse.base) {
                conversionList.add(0, Currency(symbol = it, value = value))
                return@forEach
            }

            @Suppress("IMPLICIT_CAST_TO_ANY")
            when (it) {
                Symbol.AUD -> conversionList.add(
                    Currency(
                        symbol = it,
                        value = (rates.aUD * value).toFloat()
                    )
                )
                Symbol.BGN -> conversionList.add(
                    Currency(
                        symbol = it,
                        value = (rates.bGN * value).toFloat()
                    )
                )
                Symbol.BRL -> conversionList.add(
                    Currency(
                        symbol = it,
                        value = (rates.bRL * value).toFloat()
                    )
                )
                Symbol.CAD -> conversionList.add(
                    Currency(
                        symbol = it,
                        value = (rates.cAD * value).toFloat()
                    )
                )
                Symbol.CHF -> conversionList.add(
                    Currency(
                        symbol = it,
                        value = (rates.cHF * value).toFloat()
                    )
                )
                Symbol.CNY -> conversionList.add(
                    Currency(
                        symbol = it,
                        value = (rates.cNY * value).toFloat()
                    )
                )
                Symbol.CZK -> conversionList.add(
                    Currency(
                        symbol = it,
                        value = (rates.cZK * value).toFloat()
                    )
                )
                Symbol.DKK -> conversionList.add(
                    Currency(
                        symbol = it,
                        value = (rates.dKK * value).toFloat()
                    )
                )
                Symbol.EUR -> conversionList.add(
                    Currency(
                        symbol = it,
                        value = (rates.eUR * value).toFloat()
                    )
                )
                Symbol.GBP -> conversionList.add(
                    Currency(
                        symbol = it,
                        value = (rates.gBP * value).toFloat()
                    )
                )
                Symbol.HKD -> conversionList.add(
                    Currency(
                        symbol = it,
                        value = (rates.hKD * value).toFloat()
                    )
                )
                Symbol.HRK -> conversionList.add(
                    Currency(
                        symbol = it,
                        value = (rates.hRK * value).toFloat()
                    )
                )
                Symbol.HUF -> conversionList.add(
                    Currency(
                        symbol = it,
                        value = (rates.hUF * value).toFloat()
                    )
                )
                Symbol.IDR -> conversionList.add(
                    Currency(
                        symbol = it,
                        value = (rates.iDR * value).toFloat()
                    )
                )
                Symbol.INR ->
                    Currency(
                        symbol = it,
                        value = (rates.iNR * value).toFloat()
                    )
                Symbol.ILS -> conversionList.add(
                    Currency(
                        symbol = it,
                        value = (rates.iLS * value).toFloat()
                    )
                )
                Symbol.ISK -> conversionList.add(
                    Currency(
                        symbol = it,
                        value = (rates.iSK * value).toFloat()
                    )
                )
                Symbol.JPY -> conversionList.add(
                    Currency(
                        symbol = it,
                        value = (rates.jPY * value).toFloat()
                    )
                )
                Symbol.KRW -> conversionList.add(
                    Currency(
                        symbol = it,
                        value = (rates.kRW * value).toFloat()
                    )
                )
                Symbol.MXN -> conversionList.add(
                    Currency(
                        symbol = it,
                        value = (rates.mXN * value).toFloat()
                    )
                )
                Symbol.MYR -> conversionList.add(
                    Currency(
                        symbol = it,
                        value = (rates.mYR * value).toFloat()
                    )
                )
                Symbol.NOK -> conversionList.add(
                    Currency(
                        symbol = it,
                        value = (rates.nOK * value).toFloat()
                    )
                )
                Symbol.NZD -> conversionList.add(
                    Currency(
                        symbol = it,
                        value = (rates.nZD * value).toFloat()
                    )
                )
                Symbol.PHP -> conversionList.add(
                    Currency(
                        symbol = it,
                        value = (rates.pHP * value).toFloat()
                    )
                )
                Symbol.PLN -> conversionList.add(
                    Currency(
                        symbol = it,
                        value = (rates.pLN * value).toFloat()
                    )
                )
                Symbol.RON -> conversionList.add(
                    Currency(
                        symbol = it,
                        value = (rates.rON * value).toFloat()
                    )
                )
                Symbol.RUB -> conversionList.add(
                    Currency(
                        symbol = it,
                        value = (rates.rUB * value).toFloat()
                    )
                )
                Symbol.SEK -> conversionList.add(
                    Currency(
                        symbol = it,
                        value = (rates.sEK * value).toFloat()
                    )
                )
                Symbol.SGD -> conversionList.add(
                    Currency(
                        symbol = it,
                        value = (rates.sGD * value).toFloat()
                    )
                )
                Symbol.THB -> conversionList.add(
                    Currency(
                        symbol = it,
                        value = (rates.tHB * value).toFloat()
                    )
                )
                Symbol.TRY -> conversionList.add(
                    Currency(
                        symbol = it,
                        value = (rates.tRY * value).toFloat()
                    )
                )
                Symbol.USD -> conversionList.add(
                    Currency(
                        symbol = it,
                        value = (rates.uSD * value).toFloat()
                    )
                )
                Symbol.ZAR -> conversionList.add(
                    Currency(
                        symbol = it,
                        value = (rates.zAR * value).toFloat()
                    )
                )
            }.exhaustive
        }
        return conversionList
    }
}