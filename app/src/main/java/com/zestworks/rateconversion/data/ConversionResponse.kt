package com.zestworks.rateconversion.data


import com.google.gson.annotations.SerializedName

data class ConversionResponse(
    @SerializedName("base")
    val base: Symbol, // EUR
    @SerializedName("date")
    val date: String, // 2018-09-06
    @SerializedName("rates")
    val rates: Rates
) {
    data class Rates(
        @SerializedName("AUD")
        val aUD: Double, // 1.6151
        @SerializedName("BGN")
        val bGN: Double, // 1.9542
        @SerializedName("BRL")
        val bRL: Double, // 4.7878
        @SerializedName("CAD")
        val cAD: Double, // 1.5325
        @SerializedName("CHF")
        val cHF: Double, // 1.1266
        @SerializedName("CNY")
        val cNY: Double, // 7.9385
        @SerializedName("CZK")
        val cZK: Double, // 25.694
        @SerializedName("DKK")
        val dKK: Double, // 7.4505
        @SerializedName("EUR")
        val eUR: Double, // 7.4505
        @SerializedName("GBP")
        val gBP: Double, // 0.8975
        @SerializedName("HKD")
        val hKD: Double, // 9.1249
        @SerializedName("HRK")
        val hRK: Double, // 7.428
        @SerializedName("HUF")
        val hUF: Double, // 326.22
        @SerializedName("IDR")
        val iDR: Double, // 17309.0
        @SerializedName("ILS")
        val iLS: Double, // 4.1672
        @SerializedName("INR")
        val iNR: Double, // 83.648
        @SerializedName("ISK")
        val iSK: Double, // 127.69
        @SerializedName("JPY")
        val jPY: Double, // 129.44
        @SerializedName("KRW")
        val kRW: Double, // 1303.7
        @SerializedName("MXN")
        val mXN: Double, // 22.347
        @SerializedName("MYR")
        val mYR: Double, // 4.808
        @SerializedName("NOK")
        val nOK: Double, // 9.7679
        @SerializedName("NZD")
        val nZD: Double, // 1.7618
        @SerializedName("PHP")
        val pHP: Double, // 62.54
        @SerializedName("PLN")
        val pLN: Double, // 4.3147
        @SerializedName("RON")
        val rON: Double, // 4.6347
        @SerializedName("RUB")
        val rUB: Double, // 79.509
        @SerializedName("SEK")
        val sEK: Double, // 10.582
        @SerializedName("SGD")
        val sGD: Double, // 1.5987
        @SerializedName("THB")
        val tHB: Double, // 38.099
        @SerializedName("TRY")
        val tRY: Double, // 7.6219
        @SerializedName("USD")
        val uSD: Double, // 1.1624
        @SerializedName("ZAR")
        val zAR: Double // 17.809
    )

    fun getValueForSymbol(symbol: Symbol): Double {
        if (symbol == base) {
            return 1.0
        }
        return when (symbol) {
            Symbol.AUD -> rates.aUD
            Symbol.BGN -> rates.bGN
            Symbol.BRL -> rates.bRL
            Symbol.CAD -> rates.cAD
            Symbol.CHF -> rates.cHF
            Symbol.CNY -> rates.cNY
            Symbol.CZK -> rates.cZK
            Symbol.DKK -> rates.dKK
            Symbol.EUR -> rates.eUR
            Symbol.GBP -> rates.gBP
            Symbol.HKD -> rates.hKD
            Symbol.HRK -> rates.hRK
            Symbol.HUF -> rates.hUF
            Symbol.IDR -> rates.iDR
            Symbol.ILS -> rates.iLS
            Symbol.INR -> rates.iNR
            Symbol.ISK -> rates.iSK
            Symbol.JPY -> rates.jPY
            Symbol.KRW -> rates.kRW
            Symbol.MXN -> rates.mXN
            Symbol.MYR -> rates.mYR
            Symbol.NOK -> rates.nOK
            Symbol.NZD -> rates.nZD
            Symbol.PHP -> rates.pHP
            Symbol.PLN -> rates.pLN
            Symbol.RON -> rates.rON
            Symbol.RUB -> rates.rUB
            Symbol.SEK -> rates.sEK
            Symbol.SGD -> rates.sGD
            Symbol.THB -> rates.tHB
            Symbol.TRY -> rates.tRY
            Symbol.USD -> rates.uSD
            Symbol.ZAR -> rates.zAR
        }
    }
}

enum class Symbol(symbol: String) {
    AUD("AUD"), BGN("BGN"), BRL("BRL"), CAD("CAD"), CHF("CHF"), CNY("CNY"), CZK("CZK"),
    DKK("DKK"), EUR("EUR"), GBP("GBP"), HKD("HKD"), HRK("HRK"), HUF("HUF"), IDR("IDR"),
    ILS("ILS"), INR("INR"), ISK("ISK"), JPY("JPY"), KRW("KRW"), MXN("MXN"), MYR("MYR"),
    NOK("NOK"), NZD("NZD"), PHP("PHP"), PLN("PLN"), RON("RON"), RUB("RUB"), SEK("SEK"),
    SGD("SGD"), THB("THB"), TRY("TRY"), USD("USD"), ZAR("ZAR"),
}

data class Currency(
    val symbol: Symbol,
    val value: Float
)