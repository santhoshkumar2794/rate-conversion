package com.zestworks.rateconversion.data

import com.zestworks.utils.NetworkResult
import java.io.IOException
import javax.inject.Inject

interface RatesRepository {
    suspend fun getConversionRates(base: String): NetworkResult<ConversionResponse>
}

class RatesRepositoryImpl @Inject constructor(
    private val conversionApi: ConversionApi
) : RatesRepository {

    override suspend fun getConversionRates(base: String): NetworkResult<ConversionResponse> {
        return try {
            val conversionRates = conversionApi.getConversionRates(base = base)
            if (conversionRates.isSuccessful) {
                NetworkResult.Success(conversionRates.body()!!)
            } else {
                NetworkResult.Error("OOPS!! Something went wrong.")
            }
        } catch (e: IOException) {
            NetworkResult.Error("OOPS!! Something went wrong.")
        }
    }
}