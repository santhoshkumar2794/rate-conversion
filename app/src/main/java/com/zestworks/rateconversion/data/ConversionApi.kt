package com.zestworks.rateconversion.data

import com.google.gson.GsonBuilder
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import timber.log.Timber

interface ConversionApi {

    @GET("/latest")
    suspend fun getConversionRates(@Query("base") base: String): Response<ConversionResponse>

    companion object {
        private const val BASE_URL = "https://revolut.duckdns.org/"

        fun create(okHttpClient: OkHttpClient): ConversionApi {
            val gson = GsonBuilder().setLenient().create()
            return Retrofit.Builder()
                .baseUrl(HttpUrl.parse(BASE_URL)!!)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
                .create(ConversionApi::class.java)
        }

        val okHttpClient: OkHttpClient by lazy {
            val logger = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger {
                Timber.i(it)
            })
            logger.level = HttpLoggingInterceptor.Level.BASIC

            OkHttpClient.Builder()
                .addInterceptor(logger)
                .build()
        }
    }
}