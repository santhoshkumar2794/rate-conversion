package com.zestworks.rateconversion.ui

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.zestworks.rateconversion.R
import com.zestworks.rateconversion.data.Currency
import com.zestworks.utils.load

class ConversionAdapter(
    context: Context,
    private val adapterCallback: ConversionAdapterCallback
) : ListAdapter<Currency, ConversionViewHolder>(
    object : DiffUtil.ItemCallback<Currency>() {
        override fun areItemsTheSame(oldItem: Currency, newItem: Currency): Boolean =
            oldItem.symbol == newItem.symbol

        override fun areContentsTheSame(oldItem: Currency, newItem: Currency): Boolean =
            oldItem.symbol == newItem.symbol && oldItem.value == newItem.value

    }
) {

    private val currencyNameList by lazy { load(context, R.raw.currency_names_list) }

    private val textChangeListener = object : TextWatcher {
        override fun afterTextChanged(text: Editable?) {
            text?.let {
                adapterCallback.onValueChanged(text.toString().toFloat())
            }
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ConversionViewHolder {
        return ConversionViewHolder.createViewHolder(
            parent,
            currencyNameList,
            adapterCallback,
            textChangeListener
        )
    }

    override fun onBindViewHolder(holder: ConversionViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun onCurrentListChanged(
        previousList: MutableList<Currency>,
        currentList: MutableList<Currency>
    ) {
        super.onCurrentListChanged(previousList, currentList)
        if (previousList.isNotEmpty() && currentList.isNotEmpty() && previousList[0].symbol != currentList[0].symbol){
            adapterCallback.scrollToTop()
        }
    }
}