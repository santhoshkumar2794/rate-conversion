package com.zestworks.rateconversion.ui

import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.zestworks.rateconversion.R
import com.zestworks.rateconversion.data.Currency
import com.zestworks.utils.view.load
import org.json.JSONObject

class ConversionViewHolder(
    view: View,
    private val currencyNameList: JSONObject,
    private val adapterCallback: ConversionAdapterCallback,
    private val textChangeListener: TextWatcher
) :
    RecyclerView.ViewHolder(view) {

    companion object {
        fun createViewHolder(
            parent: ViewGroup,
            currencyNameList: JSONObject,
            adapterCallback: ConversionAdapterCallback,
            textChangeListener: TextWatcher
        ): ConversionViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_conversion_view_holder, parent, false)
            return ConversionViewHolder(
                view = view,
                currencyNameList = currencyNameList,
                adapterCallback = adapterCallback,
                textChangeListener = textChangeListener
            )
        }
    }

    private val symbol by lazy { view.findViewById<ImageView>(R.id.currencySymbol) }
    private val currencyName by lazy { view.findViewById<TextView>(R.id.currency) }
    private val currencyFullName by lazy { view.findViewById<TextView>(R.id.currencyName) }
    private val currencyValue by lazy { view.findViewById<EditText>(R.id.currencyValue) }

    private var currency: Currency? = null

    fun bind(currency: Currency) {
        this.currency = currency
        currencyValue.removeTextChangedListener(textChangeListener)


        val sym = currency.symbol.name.toLowerCase().dropLast(1)
        val url = "https://www.countryflags.io/$sym/flat/64.png"
        symbol.load(url)

        currencyName.text = currency.symbol.name
        currencyFullName.text = currencyNameList.getString(currency.symbol.name)

        currencyValue.setText(currency.value.toString())

        currencyValue.imeOptions = EditorInfo.IME_ACTION_DONE
        currencyValue.isEnabled = adapterPosition == 0

        itemView.setOnClickListener {
            this.currency?.let { adapterCallback.onItemClicked(it) }
        }
        if (adapterPosition == 0) {
            currencyValue.addTextChangedListener(textChangeListener)
        }
    }
}