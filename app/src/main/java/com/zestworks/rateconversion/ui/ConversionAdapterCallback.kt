package com.zestworks.rateconversion.ui

import com.zestworks.rateconversion.data.Currency

interface ConversionAdapterCallback {

    fun onItemClicked(currency: Currency)

    fun onValueChanged(value: Float)

    fun scrollToTop()
}