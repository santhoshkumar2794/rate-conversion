package com.zestworks.rateconversion.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.zestworks.rateconversion.R
import com.zestworks.rateconversion.data.Currency
import com.zestworks.rateconversion.di.RatesInjector
import com.zestworks.rateconversion.viewmodel.*
import com.zestworks.utils.exhaustive
import kotlinx.android.synthetic.main.fragment_main.*
import timber.log.Timber
import javax.inject.Inject

class MainFragment : Fragment() {

    @Inject
    internal lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: ConversionViewModel by viewModels { viewModelFactory }

    private lateinit var snackBar: Snackbar

    private val adapter by lazy {
        ConversionAdapter(
            context = requireContext(),
            adapterCallback = object : ConversionAdapterCallback {
                override fun onItemClicked(currency: Currency) {
                    viewModel.onBaseSymbolChanged(currency)
                }

                override fun onValueChanged(value: Float) {
                    viewModel.onBaseValueChanged(value)
                }

                override fun scrollToTop() {
                    conversionView.scrollToPosition(0)
                }
            })
    }

    init {
        RatesInjector.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        snackBar = Snackbar.make(view, "", Snackbar.LENGTH_INDEFINITE)
            .setAction("RETRY") {
                viewModel.retry()
            }

        conversionView.apply {
            adapter = this@MainFragment.adapter
            layoutManager = LinearLayoutManager(context)
        }
        viewModel.ratesLiveData.observe(viewLifecycleOwner, Observer(::renderUI))
    }

    private fun renderUI(viewState: ViewState) {
        when (viewState) {
            Loading -> {
                loaderView.visibility = View.VISIBLE
                conversionView.visibility = View.GONE
            }
            is Content -> {
                loaderView.visibility = View.GONE
                conversionView.visibility = View.VISIBLE

                if (snackBar.isShown) {
                    snackBar.dismiss()
                }
                adapter.submitList(viewState.rates)
                Timber.d("Rates List: ${viewState.rates}")
            }
            is Error -> {
                snackBar.setText(viewState.errorMessage)
                snackBar.show()
                Timber.d("Error Message: ${viewState.errorMessage}")
            }
        }.exhaustive
    }
}