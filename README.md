# Rate Conversion
The app fetches exchange rates for the given currency every 1 seconds and updates.

![](https://bitbucket.org/santhoshkumar2794/rate-conversion/raw/cb0d74929c2377a64b4174d830e64e764e67819d/screenshots/Screenshot%202019-11-11%20at%209.23.13%20PM.png)

## Run the app
The project is built using AS 4.0 Canary 02 and therefore the gradle used is 4.0.0-alpha02.
If running using the other version of AS, change the gradle version to the one you're suing it

## Architecture

* MVVM + LiveData + NavHost
* Kotlin Coroutines

## Libraries Used

* Architecture Components
* Material Components
* Dagger 2
* Retrofit
* OkHttp Logging
* Timber
* Facebook Shimmer

## Test Coverage & Reports
The project has 100% test coverage for the ViewModel and the UseCase.

![](https://bitbucket.org/santhoshkumar2794/rate-conversion/raw/cb0d74929c2377a64b4174d830e64e764e67819d/screenshots/Screenshot%202019-11-11%20at%209.22.43%20PM.png)

![](https://bitbucket.org/santhoshkumar2794/rate-conversion/raw/cb0d74929c2377a64b4174d830e64e764e67819d/screenshots/Screenshot%202019-11-11%20at%209.22.52%20PM.png)